import pandas as pd
import os
#Change the dirrectories to match your computer
filestobeprocessed = []
symboliterator = 0
bigdata = pd.DataFrame()
dir= "/home/stephanie/Desktop/ScienceFair"
for root, dirs,files in os.walk(r"/home/stephanie/Desktop/ScienceFair/CsvfolderneedsprocessB"):
    for file in files:
        if file.endswith(".csv"):
            filestobeprocessed.append(file)
while symboliterator < len(filestobeprocessed):
    print(filestobeprocessed[symboliterator])
    data_frame = pd.read_csv("/home/stephanie/Desktop/ScienceFair/CsvfolderneedsprocessB/" + filestobeprocessed[symboliterator] )
    data_frame = data_frame.set_index('timestamp')
    data_frame = data_frame.assign(symbol="")
    #print(data_frame[(data_frame.index > '1998-08-10') & (data_frame.index <= '1998-12-25')].head)
    if data_frame[(data_frame.index > '1998-01-02') & (data_frame.index <= '1998-01-10')].empty != True and data_frame[(data_frame.index > '1998-12-21') & (data_frame.index <= '1998-12-31')].empty != True:
        df98 = data_frame[(data_frame.index > '1998-01-02') & (data_frame.index <= '1998-01-10')]
        df98 = df98.assign(symbol=filestobeprocessed[symboliterator])
        df98.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "98.csv")
    if data_frame[(data_frame.index > '1999-01-04') & (data_frame.index <= '1999-01-10')].empty!= True and data_frame[(data_frame.index > '1999-12-21') & (data_frame.index <= '1999-12-31')].empty != True:
        df99 = data_frame[(data_frame.index > '1999-01-04') & (data_frame.index <= '1999-01-10')]
        df99 = df99.assign(symbol=filestobeprocessed[symboliterator])
        df99.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "99.csv")    
    if data_frame[(data_frame.index > '2000-01-01') & (data_frame.index <= '2000-01-10')].empty!= True and data_frame[(data_frame.index > '2000-01-21') & (data_frame.index <= '2000-12-31')].empty!= True:
        df00 = data_frame[(data_frame.index > '2000-01-01') & (data_frame.index <= '2000-12-31')]
        df00 = df00.assign(symbol=filestobeprocessed[symboliterator])
        df00.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "00.csv")
    if data_frame[(data_frame.index > '2001-01-01') & (data_frame.index <= '2001-01-10')].empty!= True and data_frame[(data_frame.index > '2001-01-21') & (data_frame.index <= '2001-12-31')].empty!= True:
        df01 = data_frame[(data_frame.index > '2001-01-01') & (data_frame.index <= '2001-12-31')]
        df01 = df01.assign(symbol=filestobeprocessed[symboliterator])
        df01.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "01.csv")
    if data_frame[(data_frame.index > '2002-01-01') & (data_frame.index <= '2002-01-10')].empty!= True and data_frame[(data_frame.index > '2002-01-21') & (data_frame.index <= '2002-12-31')].empty!= True:
        df02 = data_frame[(data_frame.index > '2002-01-01') & (data_frame.index <= '2002-12-31')]
        df02 = df02.assign(symbol=filestobeprocessed[symboliterator])
        df02.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "02.csv")
    if data_frame[(data_frame.index > '2003-01-01') & (data_frame.index <= '2003-01-10')].empty!= True and data_frame[(data_frame.index > '2003-01-21') & (data_frame.index <= '2003-12-31')].empty!= True:
        df03 = data_frame[(data_frame.index > '2003-01-01') & (data_frame.index <= '2003-12-31')]
        df03 = df03.assign(symbol=filestobeprocessed[symboliterator])
        df03.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "03.csv")
    if data_frame[(data_frame.index > '2004-01-01') & (data_frame.index <= '2004-01-10')].empty!= True and data_frame[(data_frame.index > '2004-01-21') & (data_frame.index <= '2004-12-31')].empty!= True: 
        df04 = data_frame[(data_frame.index > '2004-01-01') & (data_frame.index <= '2004-12-31')]
        df04 = df04.assign(symbol=filestobeprocessed[symboliterator])
        df04.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "04.csv")
    if data_frame[(data_frame.index > '2005-01-01') & (data_frame.index <= '2005-01-10')].empty!= True and data_frame[(data_frame.index > '2005-01-21') & (data_frame.index <= '2005-12-31')].empty!= True:
        df05 = data_frame[(data_frame.index > '2005-01-01') & (data_frame.index <= '2005-12-31')]
        df05 = df05.assign(symbol=filestobeprocessed[symboliterator])
        df05.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "05.csv")
    if data_frame[(data_frame.index > '2006-01-01') & (data_frame.index <= '2006-01-10')].empty!= True and data_frame[(data_frame.index > '2006-01-21') & (data_frame.index <= '2006-12-31')].empty!= True:
        df06 = data_frame[(data_frame.index > '2006-01-01') & (data_frame.index <= '2006-12-31')]
        df06 = df06.assign(symbol=filestobeprocessed[symboliterator])
        df06.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "06.csv")
    if data_frame[(data_frame.index > '2007-01-01') & (data_frame.index <= '2007-01-10')].empty!= True and data_frame[(data_frame.index > '2007-01-21') & (data_frame.index <= '2007-12-31')].empty!= True:
        df07 = data_frame[(data_frame.index > '2007-01-01') & (data_frame.index <= '2007-12-31')]
        df07 = df07.assign(symbol=filestobeprocessed[symboliterator])
        df07.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "07.csv")
    if data_frame[(data_frame.index > '2008-01-01') & (data_frame.index <= '2008-01-10')].empty!= True and data_frame[(data_frame.index > '2008-01-21') & (data_frame.index <= '2008-12-31')].empty!= True:
        df08 = data_frame[(data_frame.index > '2008-01-01') & (data_frame.index <= '2008-12-31')]
        df08 = df08.assign(symbol=filestobeprocessed[symboliterator])
        df08.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "08.csv")
    if data_frame[(data_frame.index > '2009-01-01') & (data_frame.index <= '2009-01-10')].empty!= True and data_frame[(data_frame.index > '2009-01-21') & (data_frame.index <= '2009-12-31')].empty!= True:
        df09 = data_frame[(data_frame.index > '2009-01-01') & (data_frame.index <= '2009-12-31')]
        df09 = df09.assign(symbol=filestobeprocessed[symboliterator])
        df09.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "09.csv")
    if data_frame[(data_frame.index > '2010-01-01') & (data_frame.index <= '2010-01-10')].empty!= True and data_frame[(data_frame.index > '2010-01-21') & (data_frame.index <= '2010-12-31')].empty!= True:
        df10 = data_frame[(data_frame.index > '2010-01-01') & (data_frame.index <= '2010-12-31')]
        df10 = df10.assign(symbol=filestobeprocessed[symboliterator])
        df10.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "10.csv")
    if data_frame[(data_frame.index > '2011-01-01') & (data_frame.index <= '2011-01-10')].empty!= True and data_frame[(data_frame.index > '2011-01-21') & (data_frame.index <= '2011-12-31')].empty!= True:
        df11 = data_frame[(data_frame.index > '2011-01-01') & (data_frame.index <= '2011-12-31')]
        df11 = df11.assign(symbol=filestobeprocessed[symboliterator])
        df11.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "11.csv")
    if data_frame[(data_frame.index > '2012-01-01') & (data_frame.index <= '2012-01-10')].empty!= True and data_frame[(data_frame.index > '2012-01-21') & (data_frame.index <= '2012-12-31')].empty!= True:
        df12 = data_frame[(data_frame.index > '2012-01-01') & (data_frame.index <= '2012-12-31')]
        df12 = df12.assign(symbol=filestobeprocessed[symboliterator])
        df12.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "12.csv")
    if data_frame[(data_frame.index > '2013-01-01') & (data_frame.index <= '2013-01-10')].empty!= True and data_frame[(data_frame.index > '2013-01-21') & (data_frame.index <= '2013-12-31')].empty!= True:
        df13 = data_frame[(data_frame.index > '2013-01-01') & (data_frame.index <= '2013-12-31')]
        df13 = df13.assign(symbol=filestobeprocessed[symboliterator])
        df13.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "13.csv")
    if data_frame[(data_frame.index > '2014-01-01') & (data_frame.index <= '2014-01-10')].empty!= True and data_frame[(data_frame.index > '2014-01-21') & (data_frame.index <= '2014-12-31')].empty!= True:
        df14 = data_frame[(data_frame.index > '2014-01-01') & (data_frame.index <= '2014-12-31')]
        df14 = df14.assign(symbol=filestobeprocessed[symboliterator])
        df14.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "14.csv")
    if data_frame[(data_frame.index > '2015-01-01') & (data_frame.index <= '2015-01-10')].empty!= True and data_frame[(data_frame.index > '2015-01-21') & (data_frame.index <= '2015-12-31')].empty!= True:
        df15 = data_frame[(data_frame.index > '2015-01-01') & (data_frame.index <= '2015-12-31')]
        df15 = df15.assign(symbol=filestobeprocessed[symboliterator])
        df15.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "15.csv")
    if data_frame[(data_frame.index > '2016-01-01') & (data_frame.index <= '2016-01-10')].empty!= True and data_frame[(data_frame.index > '2016-01-21') & (data_frame.index <= '2016-12-31')].empty!= True:
        df16 = data_frame[(data_frame.index > '2016-01-01') & (data_frame.index <= '2016-12-31')]  
        df16 = df16.assign(symbol=filestobeprocessed[symboliterator])
        df16.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "16.csv")
    if data_frame[(data_frame.index > '2017-01-01') & (data_frame.index <= '2017-01-10')].empty!= True and data_frame[(data_frame.index > '2017-01-21') & (data_frame.index <= '2017-12-31')].empty!= True:
        df17 = data_frame[(data_frame.index > '2017-01-01') & (data_frame.index <= '2017-12-31')]
        df17 = df17.assign(symbol=filestobeprocessed[symboliterator])
        df17.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "17.csv")
    if data_frame[(data_frame.index > '2018-01-01') & (data_frame.index <= '2018-01-10')].empty!= True and data_frame[(data_frame.index > '2018-01-21') & (data_frame.index <= '2018-12-31')].empty!= True:
        df18 = data_frame[(data_frame.index > '2018-01-01') & (data_frame.index <= '2018-12-31')]
        df18 = df18.assign(symbol=filestobeprocessed[symboliterator])
        df18.to_csv(dir+"/ProcessC/" + filestobeprocessed[symboliterator]+ "18.csv")
    symboliterator+=1
print(bigdata.head)
    