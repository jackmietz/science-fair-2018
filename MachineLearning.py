import pandas as pd
import os
import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
ScoreDF = pd.DataFrame(data = {"Iteration": [], "LassoScore": [], "ElasticNetScore": []})
X_train_pre = pd.read_csv(r"/home/stephanie/Desktop/ScienceFair/train1lessdata.csv")
y_train = X_train_pre[["close"]]
X_train = X_train_pre.drop("close", axis = 1)
X_test_pre = pd.read_csv("/home/stephanie/Desktop/ScienceFair/testlessdata.csv")
y_test = X_test_pre[["close"]]
X_test = X_test_pre.drop("close", axis = 1)

#print(X_train.columns.values)
#print(X_test.columns.values)
#print(y_test.columns.values)
#print(y_train.columns.values)
iteratorrules_pre = pd.read_csv("/home/stephanie/Desktop/ScienceFair/IteratorRules.csv")
iteratorrules = iteratorrules_pre.values.tolist()
iterator = 1
iteration = 0
from sklearn.preprocessing import LabelEncoder
lb_date = LabelEncoder()
X_test["timestamp"] = lb_date.fit_transform(X_test["timestamp"])
lb_symbol = LabelEncoder()
X_test["symbol"] = lb_symbol.fit_transform(X_test["symbol"])
lb1_date = LabelEncoder()
X_train["timestamp"] = lb1_date.fit_transform(X_train["timestamp"])
lb1_symbol = LabelEncoder()
X_train["symbol"] = lb1_symbol.fit_transform(X_train["symbol"])
# There are three steps to model something with sklearn
# 1. Set up the model
while iterator < len(iteratorrules):
	iteration = iteratorrules_pre.ix[iterator]
	#print(iteration)
	model = Lasso(max_iter  = iteration)
	# 2. Use fit
	model.fit(X_train, y_train)
	# 3. Check the score
	modelYPred = model.predict(X_test)
	#pd.DataFrame(modelYPred).to_csv("/home/stephanie/Desktop/LassoPredictions.csv",header=False, index= False)
	model_score = mean_squared_error(y_test,modelYPred)
	# There are three steps to model something with sklearn
	# 1. Set up the model
	model1 = ElasticNet(max_iter = iteration)
	# 2. Use fit
	model1.fit(X_train, y_train)
	# 3. Check the score
	model1YPred = model1.predict(X_test)
	#pd.DataFrame(model1YPred).to_csv("/home/stephanie/Desktop/ElasticNetPredictions.csv", header = False,index = False)

	model1_score = mean_squared_error(y_test,model1YPred)
	#print(r"Iteration: "+str(iteration)+" | Lasso Score: " + str(model_score) + " | ElasticNet Score: " + str(model1_score)  )
	ScoreDF = ScoreDF.append(pd.DataFrame(data = {"Iteration": [iteration], "LassoScore": [model_score], "ElasticNetScore": [model1_score]}))
	#iterator = iterator + 1
	#print(ScoreDF)
ScoreDF.to_csv("/home/stephanie/Desktop/ScienceFair/results1.csv")
print("Done")