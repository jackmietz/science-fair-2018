import time
import csv
import requests
import os
apikey= "" # Provide your own api key
symboliterator = 0
totaltoday=input("How many stock today? | (Default = 499)")
if totaltoday != "" :
    totaltoday=int(totaltoday)
else:
    totaltoday = 499
totaltodayunchange = totaltoday
def initializesymbollist():
    global symbollist
    with open('symlist2.csv', 'r') as f:
        reader = csv.reader(f)
        symbollist_pre = list(reader)
        symbollist = []
        for i in symbollist_pre:
            symbollist.extend(i)
initializesymbollist()
path = r"/home/pi/ScienceFair/csvfile"
if not os.path.exists(path):
    os.makedirs(path)
def deleteline():
    global symbollist
    with open('symlist2.csv', 'r') as fin:
        data = fin.read().splitlines(True)
    with open('symlist2.csv', 'w') as fout:
        fout.writelines(data[1:])
    initializesymbollist()
while (symboliterator < len(symbollist) and totaltoday>0):
    parameter = {"function": "TIME_SERIES_DAILY", "symbol": symbollist[symboliterator], "outputsize": "full", "datatype": "csv", "apikey": apikey}
    print("Current Symbol: " + str(symbollist[symboliterator]) + "| Progress: " + str(symboliterator) + r"/" + str(totaltodayunchange) + " | Remaining: " + str(totaltoday) )
    print("Starting Timer")
    time.sleep(12)
    print("Timer Done")
    test = requests.get("https://www.alphavantage.co/query", params=parameter)
    filename=symbollist[symboliterator] + ".csv"
    file = open( os.path.join(path, filename), "w+")
    file.write(test.text)
    file.close
    symboliterator += 1
    totaltoday -= 1
    deleteline()
print("Program Done")
